Web application developed with Spring Boot that allow students to apply to listed internships offers by approved companies.

Authors:

- Stefano Valentini https://gitlab.com/valent0ne
- Valentina Cecchini https://gitlab.com/Nimerya
- Federico Battista
- Daniele Berardini